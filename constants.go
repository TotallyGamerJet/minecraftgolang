package main

import "github.com/go-gl/mathgl/mgl32"

const(
	width = 800
	height = 600
	title = "Minecraft"
	fov float32 = 70
	nearPlane float32 = 0.1
	farPlane float32 = 1000
	runSpeed = 2.1585
	turnSpeed = 10
)

var blocks map[TexturedModel][]Block

var location_transformation int32
var location_projection int32
var location_view int32

var projection mgl32.Mat4

var camera Camera
var program uint32

var lastFrameTime float64
var delta float64

var currentSpeed float64 = 0
var currentStrafeSpeed float64 = 0

var vertices = []float32{
	-0.5,0.5,-0.5,
	-0.5,-0.5,-0.5,
	0.5,-0.5,-0.5,
	0.5,0.5,-0.5,

	-0.5,0.5,0.5,
	-0.5,-0.5,0.5,
	0.5,-0.5,0.5,
	0.5,0.5,0.5,

	0.5,0.5,-0.5,
	0.5,-0.5,-0.5,
	0.5,-0.5,0.5,
	0.5,0.5,0.5,

	-0.5,0.5,-0.5,
	-0.5,-0.5,-0.5,
	-0.5,-0.5,0.5,
	-0.5,0.5,0.5,

	-0.5,0.5,0.5,
	-0.5,0.5,-0.5,
	0.5,0.5,-0.5,
	0.5,0.5,0.5,

	-0.5,-0.5,0.5,
	-0.5,-0.5,-0.5,
	0.5,-0.5,-0.5,
	0.5,-0.5,0.5,
}

var textureCoords = []float32{
	0,0,
	0,1,
	1,1,
	1,0,
	0,0,
	0,1,
	1,1,
	1,0,
	0,0,
	0,1,
	1,1,
	1,0,
	0,0,
	0,1,
	1,1,
	1,0,
	0,0,
	0,1,
	1,1,
	1,0,
	0,0,
	0,1,
	1,1,
	1,0,
}

var indices = []int32{
	0,1,3,
	3,1,2,
	4,5,7,
	7,5,6,
	8,9,11,
	11,9,10,
	12,13,15,
	15,13,14,
	16,17,19,
	19,17,18,
	20,21,23,
	23,21,22,
}

var vertexShader = `
#version 400 core

in vec3 position;
in vec2 textureCoords;

out vec2 pass_textureCoords;

uniform mat4 transformationMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

void main(void) {

    gl_Position = projectionMatrix * viewMatrix * transformationMatrix * vec4(position.xyz,1.0);
    pass_textureCoords = textureCoords;

}
` + "\x00"

var fragmentShader = `
#version 400 core

in vec2 pass_textureCoords;

out vec4 out_Color;

uniform sampler2D textureSampler;

void main(void) {

    out_Color = texture(textureSampler,pass_textureCoords);

}
` + "\x00"
