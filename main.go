package main

import (
	"github.com/go-gl/glfw/v3.2/glfw"
	"github.com/go-gl/gl/v4.1-compatibility/gl"
	"runtime"
	"log"
	"fmt"
	"strings"
	"os"
	_"image/png"
	"image/draw"
	"image"
	"github.com/go-gl/mathgl/mgl32"
	"math"
)

func init() {
	// GLFW event handling must run on the main OS thread
	runtime.LockOSThread()
}

func move(w *glfw.Window, key glfw.Key, scancode int, action glfw.Action, mods glfw.ModifierKey) {
	if key == glfw.KeyW && action == glfw.Press || action == glfw.Repeat && key == glfw.KeyW {
		currentSpeed = runSpeed
		distance := currentSpeed * delta
		dx := distance * math.Sin(float64(degreesToRadians(camera.yaw)))
		dz := distance * math.Cos(float64(degreesToRadians(camera.yaw)))
		camera.position = camera.position.Add(mgl32.Vec3{float32(dx), 0, float32(-dz)})
	}else if key == glfw.KeyS && action == glfw.Press || action == glfw.Repeat && key == glfw.KeyS {
		currentSpeed = runSpeed
		distance := currentSpeed * delta
		dx := distance * math.Sin(float64(degreesToRadians(camera.yaw)))
		dz := distance * math.Cos(float64(degreesToRadians(camera.yaw)))
		camera.position = camera.position.Add(mgl32.Vec3{float32(-dx), 0, float32(dz)})
	} else if key == glfw.KeyA && action == glfw.Press || action == glfw.Repeat && key == glfw.KeyA {
		currentStrafeSpeed = runSpeed
		distance := currentStrafeSpeed * delta
		dx := distance * math.Sin(float64(degreesToRadians(camera.yaw + 90)))
		dz := distance * math.Cos(float64(degreesToRadians(camera.yaw + 90)))
		camera.position = camera.position.Add(mgl32.Vec3{float32(-dx), 0, float32(dz)})
	} else if key == glfw.KeyD && action == glfw.Press || action == glfw.Repeat && key == glfw.KeyD {
		currentStrafeSpeed = runSpeed
		distance := currentStrafeSpeed * delta
		dx := distance * math.Sin(float64(degreesToRadians(camera.yaw + 90)))
		dz := distance * math.Cos(float64(degreesToRadians(camera.yaw + 90)))
		camera.position = camera.position.Add(mgl32.Vec3{float32(dx), 0, float32(-dz)})
	} else {
		currentSpeed = 0
		currentStrafeSpeed = 0
	}

	if key == glfw.KeySpace && action == glfw.Press || action == glfw.Repeat && key == glfw.KeySpace  {
		camera.position = camera.position.Add(mgl32.Vec3{0, 0.02, 0})
	}
	if key == glfw.KeyLeftShift && action == glfw.Press || action == glfw.Repeat && key == glfw.KeyLeftShift  {
		camera.position = camera.position.Add(mgl32.Vec3{0, -0.02, 0})
	}
	if key == glfw.KeyEscape && action == glfw.Press {
		w.SetInputMode(glfw.CursorMode, glfw.CursorNormal)
	}
}

func mouse(w *glfw.Window, xpos, ypos float64) {
//	var lastTime float64
	//currentTime := glfw.GetTime()
	//deltaTime := float32(currentTime - lastTime)
//	lastTime = currentTime

	//moves camera
	camera.yaw = -turnSpeed * float32(delta) * float32(width/2 - xpos)

	camera.pitch = -turnSpeed * float32(delta) * float32(height/2 - ypos)
	if camera.pitch > 90 {
		camera.pitch = 90
	} else if camera.pitch < -90 {
		camera.pitch = -90
	}
}

func main() {

	if err := glfw.Init(); err != nil {
		log.Fatalln("failed to initialize glfw:", err)
	}
	defer glfw.Terminate()

	window, _ := createDisplay()

	defer closeDisplay(window)

	// Initialize Glow
	if err := gl.Init(); err != nil {
		panic(err)
	}

	version := gl.GoStr(gl.GetString(gl.VERSION))
	fmt.Println("OpenGL version", version)

	// Configure the vertex and fragment shaders
	program, err := newProgram(vertexShader, fragmentShader)
	if err != nil {
		panic(err)
	}

	gl.UseProgram(program)
	defer gl.UseProgram(0)

	location_transformation = gl.GetUniformLocation(program, gl.Str("transformationMatrix\x00"))
	location_projection = gl.GetUniformLocation(program, gl.Str("projectionMatrix\x00"))
	location_view = gl.GetUniformLocation(program, gl.Str("viewMatrix\x00"))

	projection = createProjectionMatrix()
	loadMatrix(location_projection, &projection)//Ep. 8 11:11

	model := createModel(vertices, textureCoords, indices, "stone.png")
	var blockList [16][16]Block
		for y := 0; y < 16; y++ {
			for x := 0; x < 16; x++ {
				block := Block{model, mgl32.Vec3{float32(x) * float32(0.5), 0, float32(y) * float32(-0.5)}, 0, 0, 0, 0.5}
				blockList[x][y] = block
			}
		}
	camera = Camera{mgl32.Vec3{0, 1.5, 0}, 0, 0}

	window.SetKeyCallback(move)
	window.SetSizeCallback(onResize)
	window.SetCursorPosCallback(mouse)
	window.SetInputMode(glfw.CursorMode, glfw.CursorDisabled)

	for !window.ShouldClose() {
		prepare()

		for _, blocks := range blockList {
			for _, block := range blocks {
				processEntity(block)
			}
		}

		renderBlocks(blocks)
		//glfw.SwapInterval(0)
		updateDisplay()
		// Maintenance
		window.SwapBuffers()
	}

}
//RENDERER
func prepare() {
	gl.Enable(gl.DEPTH_TEST)
	//gl.Enable(gl.CULL_FACE)
	gl.ClearColor(1.0, 0, 0, 1.0)
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
	glfw.PollEvents()
	viewMatrix := createViewMatrix(camera)//THIS IS PROBABLY WRONG
	loadMatrix(location_view, &viewMatrix)
	blocks = make(map[TexturedModel][]Block)

}

func renderBlocks(blocks map[TexturedModel][]Block) {
	for texturedmodel := range blocks {
		prepareTexturedModel(texturedmodel)
		batch := blocks[texturedmodel]
		for _, block := range batch {
			prepareInstance(block)
			gl.DrawElements(gl.TRIANGLES, block.texturedmodel.rawModel.vertexCount, gl.UNSIGNED_INT, gl.PtrOffset(0))
		}
		unbindTexturedModel()
	}
	blocks = nil
}

func processEntity(block Block) {
	model := block.texturedmodel
	batch := blocks[model]
	if batch != nil {
		batch = append(batch, block)
		blocks[model] = batch
	} else {
		var batch = make([]Block, 16)
		batch[0] = block
		blocks[model] = batch
	}
}

func prepareTexturedModel(texture TexturedModel) {
	model := texture.rawModel
	gl.BindVertexArray(model.vaoID)
	//vertAttrib := uint32(gl.GetAttribLocation(program, gl.Str("vert\x00")))
	gl.EnableVertexAttribArray(0)
	gl.EnableVertexAttribArray(1)

	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, texture.textureID)
}

func unbindTexturedModel() {
	gl.DisableVertexAttribArray(0)
	gl.DisableVertexAttribArray(1)
	gl.BindVertexArray(0)
}

func prepareInstance(block Block) {
	transformationMatrix := createTransformationMatrix(block.position, block.rotX, block.rotY, block.rotZ, block.scale)
	loadMatrix(location_transformation, &transformationMatrix)
}

func render(block Block) {
	texturedmodel := block.texturedmodel
	model := block.texturedmodel.rawModel
	gl.BindVertexArray(model.vaoID)
	//vertAttrib := uint32(gl.GetAttribLocation(program, gl.Str("vert\x00")))
	gl.EnableVertexAttribArray(0)
	gl.EnableVertexAttribArray(1)

	transformationMatrix := createTransformationMatrix(block.position, block.rotX, block.rotY, block.rotZ, block.scale)
	loadMatrix(location_transformation, &transformationMatrix)
	viewMatrix := createViewMatrix(camera)
	loadMatrix(location_view, &viewMatrix)

	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, texturedmodel.textureID)
	//gl.DrawArrays(gl.TRIANGLES, 0, model.vertexCount)
	gl.DrawElements(gl.TRIANGLES, model.vertexCount, gl.UNSIGNED_INT, gl.PtrOffset(0))
	gl.DisableVertexAttribArray(0)
	gl.DisableVertexAttribArray(1)
	gl.BindVertexArray(0)
}

func onResize(w *glfw.Window, width, height int) {
	aspectRatio := float32(width)/float32(height)
	projection = mgl32.Perspective(mgl32.DegToRad(fov), aspectRatio, nearPlane, farPlane)
	loadMatrix(location_projection, &projection)

	gl.Viewport(0, 0, int32(width), int32(height))
}

func createProjectionMatrix() mgl32.Mat4{
	aspectRatio := float32(width)/height
	return mgl32.Perspective(mgl32.DegToRad(fov), aspectRatio, nearPlane, farPlane)
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}
//DISPLAY

func createDisplay() (*glfw.Window, error) {
	glfw.WindowHint(glfw.Resizable, glfw.True)
	glfw.WindowHint(glfw.ContextVersionMajor, 4)
	glfw.WindowHint(glfw.ContextVersionMinor, 2)
	glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)
	glfw.WindowHint(glfw.OpenGLForwardCompatible, glfw.True)
	window, err := glfw.CreateWindow(width, height, title, nil, nil)
	check(err)
	window.MakeContextCurrent()
	lastFrameTime = glfw.GetTime()
	return window, err
}
var nbFrames = 0
var lastTime float64 = 0
func updateDisplay() {
	currentFrameTime := glfw.GetTime()
	delta = currentFrameTime - lastFrameTime //might need to change from miliseconds to seconds by dividing by 1000
	lastFrameTime = currentFrameTime
	FPSCounter()
}

func FPSCounter() {
	currentTime := glfw.GetTime()
	nbFrames++
	if currentTime - lastTime >= 1.0 {
		fmt.Printf("%f ms/frame\n", 1000.0/float64(nbFrames))
		nbFrames = 0
		lastTime += 1.0
	}
}

func closeDisplay(window *glfw.Window) {
	window.Destroy()
}

//LOADER
func createModel(positions, textureCoords []float32, indices []int32, texture string) TexturedModel {
	model := loadToVAO(positions,textureCoords, indices)
	textureID, err := loadTexture(texture)
	check(err)
	texturedModel := TexturedModel{model, textureID}
	return texturedModel
}

func loadToVAO(positions, textureCoords []float32, indices []int32) RawModel {
	vaoID := createVAO()
	bindIndicesBuffer(indices)
	storeDataInAttributeLists(0, 3, positions)
	storeDataInAttributeLists(1, 2, textureCoords)
	unbindVAO()
	return RawModel{vaoID, int32(len(indices))} //THIS int32(...) might be wrong!
}

func loadTexture(file string) (uint32, error) {
	imgFile, err := os.Open(file)
	if err != nil {
		return 0, fmt.Errorf("texture %q not found on disk: %v", file, err)
	}
	img, _, err := image.Decode(imgFile)
	if err != nil {
		return 0, err
	}

	rgba := image.NewRGBA(img.Bounds())
	if rgba.Stride != rgba.Rect.Size().X*4 {
		return 0, fmt.Errorf("unsupported stride")
	}
	draw.Draw(rgba, rgba.Bounds(), img, image.Point{0, 0}, draw.Src)

	var texture uint32
	gl.GenTextures(1, &texture)
	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, texture)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
	gl.TexImage2D(
		gl.TEXTURE_2D,
		0,
		gl.RGBA,
		int32(rgba.Rect.Size().X),
		int32(rgba.Rect.Size().Y),
		0,
		gl.RGBA,
		gl.UNSIGNED_BYTE,
		gl.Ptr(rgba.Pix))

	return texture, nil
}

func createVAO() uint32 {
	var vao uint32
	gl.GenVertexArrays(1, &vao)
	//SHOULD I ADD vao TO A SLICE FOR MEMORY MANAGEMENT? -EP 2 11:25
	gl.BindVertexArray(vao)
	return vao
}

func storeDataInAttributeLists(attrib uint32, coordSize int32, data []float32) {
	var vbo uint32
	gl.GenBuffers(1, &vbo)
	//SHOULD I ADD vbo TO A SLICE FOR MEMORY MANAGEMENT? -EP 2 11:25
	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
	gl.BufferData(gl.ARRAY_BUFFER, len(data)*4, gl.Ptr(data), gl.STATIC_DRAW)

	gl.VertexAttribPointer(attrib, coordSize, gl.FLOAT, false, 0/**(5*4)*/, gl.PtrOffset(0))

	gl.BindBuffer(gl.ARRAY_BUFFER, 0)
}

func unbindVAO() {
	gl.BindVertexArray(0)
}

func bindIndicesBuffer(indices []int32) {
	var vbo uint32
	gl.GenBuffers(1, &vbo)
	//SHOULD I ADD vbo TO A SLICE FOR MEMORY MANAGEMENT? -EP 3 7:00
	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, vbo)
	gl.BufferData(gl.ELEMENT_ARRAY_BUFFER, len(indices)*4, gl.Ptr(indices), gl.STATIC_DRAW)
}

//ShaderProgram
func newProgram(vertexShaderSource, fragmentShaderSource string) (uint32, error) {
	vertexShader, err := compileShader(vertexShaderSource, gl.VERTEX_SHADER)
	if err != nil {
		return 0, err
	}

	fragmentShader, err := compileShader(fragmentShaderSource, gl.FRAGMENT_SHADER)
	if err != nil {
		return 0, err
	}

	program := gl.CreateProgram()

	gl.AttachShader(program, vertexShader)
	gl.AttachShader(program, fragmentShader)
	//MIGHT NEED TO BINDATTRIBUTES HERE INSTEAD OF ELSE WHERE
	bindAllAttributes()
	gl.LinkProgram(program)
	gl.ValidateProgram(program)
	//getAllUniformLocations(program)
	/**var status int32
	gl.GetProgramiv(program, gl.LINK_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetProgramiv(program, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetProgramInfoLog(program, logLength, nil, gl.Str(log))

		return 0, fmt.Errorf("failed to link program: %v", log)
	}*/

	gl.DeleteShader(vertexShader)
	gl.DeleteShader(fragmentShader)

	return program, nil
}

func bindAllAttributes() {
	gl.BindAttribLocation(program, 0, gl.Str("position\x00"))
	gl.BindAttribLocation(program, 1, gl.Str("textureCoords\x00"))
}

/**func loadTransformationMatrix(matrix *mgl32.Mat4) {
	loadMatrix(projectionUniform, matrix)
}*/
/**var tranformationLocation int32
func getAllUniformLocations(program uint32) {
	//Ep. 7 8:48
	location_transformation = getUniformLocation(program, "transformation\x00")
}*/

/**func getUniformLocation(program uint32, uniformName string) int32 {
	return gl.GetUniformLocation(program, gl.Str(uniformName))
}*/

/*func bindAttributes(program, attribute uint32, variableName string) {
	gl.BindAttribLocation(program, attribute, gl.Str(variableName))
}*/

func loadFloat(location int32, value float32) {
	gl.Uniform1f(location, value)
}

func loadVector(location int32, vector mgl32.Vec3) {
	gl.Uniform3f(location, vector.X(), vector.Y(), vector.Z())
}

func loadBoolean(location int32, value bool) {
	var toLoad float32 = 0
	if value {
		toLoad = 1
	}
	gl.Uniform1f(location, toLoad)
}

func loadMatrix(location int32, matrix *mgl32.Mat4) {
	gl.UniformMatrix4fv(location,1,false, &matrix[0])
}

func compileShader(source string, shaderType uint32) (uint32, error) {
	shader := gl.CreateShader(shaderType)

	csources, free := gl.Strs(source)
	gl.ShaderSource(shader, 1, csources, nil)
	free()
	gl.CompileShader(shader)

	var status int32
	gl.GetShaderiv(shader, gl.COMPILE_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetShaderiv(shader, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetShaderInfoLog(shader, logLength, nil, gl.Str(log))

		return 0, fmt.Errorf("failed to compile %v: %v", source, log)
	}

	return shader, nil
}
//Maths
func degreesToRadians(v float32) float32 {
	return v * (math.Pi / 180)
}

func radiansToDegrees(v float32) float32 {
	return v * (180 / math.Pi)
}

func createTransformationMatrix(translation mgl32.Vec3, rx, ry, rz, scale float32) mgl32.Mat4 {
	matrix := mgl32.Ident4()
	matrix = matrix.Mul4(mgl32.Translate3D(translation.X(), translation.Y(), translation.Z()))
	matrix = matrix.Mul4(mgl32.HomogRotate3DX(rx))
	matrix = matrix.Mul4(mgl32.HomogRotate3DY(ry))
	matrix = matrix.Mul4(mgl32.HomogRotate3DZ(rz))
	matrix = matrix.Mul4(mgl32.Scale3D(scale, scale, scale))
	return matrix
}

func createViewMatrix(c Camera) mgl32.Mat4 {
	matrix := mgl32.Ident4()
	matrix = matrix.Mul4(mgl32.HomogRotate3DX(degreesToRadians(c.pitch)))
	matrix = matrix.Mul4(mgl32.HomogRotate3DY(degreesToRadians(c.yaw)))
	matrix = matrix.Mul4(mgl32.Translate3D(-c.position.X(), -c.position.Y(), -c.position.Z()))
	return matrix
}