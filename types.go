package main

import "github.com/go-gl/mathgl/mgl32"

type Camera struct {
	position mgl32.Vec3
	pitch, yaw float32
}

type Block struct {
	texturedmodel TexturedModel
	position mgl32.Vec3
	rotX, rotY, rotZ, scale float32
}

type TexturedModel struct {
	rawModel RawModel
	textureID uint32
}

type RawModel struct {
	vaoID uint32
	vertexCount int32
}
